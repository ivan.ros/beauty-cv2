<?php

namespace Main\BeautyCVBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('MainBeautyCVBundle:home:index.html.twig');
    }
}
