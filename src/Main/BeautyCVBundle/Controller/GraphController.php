<?php

namespace Main\BeautyCVBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GraphController extends Controller
{
    public function indexAction()
    {
        return $this->render('MainBeautyCVBundle:graphs:index.html.twig');
    }
}
